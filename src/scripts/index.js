import '../styles/index.scss';

import { tariffs } from './constants';
import { sabHeaders} from './constants';

let balance = 100;

const tabs = document.querySelectorAll('.left__company');
const heder = document.querySelector('.center__title');
const meters = document.querySelector('#meters');
const previous = document.querySelector('#previous');
const current = document.querySelector('#current');
const btnSave = document.querySelector('#save');
const btnReset = document.querySelector('[type="reset"]');
const wrapList = document.querySelector('.form__summary-list');
const btnPay = document.querySelector('#pay');
const saveId = document.querySelector('.right__payments-fields');
const transactions = document.querySelector('.transactions__list');
const cubHeder = document.querySelector('.center__desc');

const payment = {
    id: null,
    meterId: null,
    current: null,
    previous: null,
    amount: null,
};

const payments = (JSON.parse(localStorage.getItem('payments')) === null)? 
[]: JSON.parse(localStorage.getItem('payments'));
addId();
addChecked();
domList ();
metersAdd();

tabs.forEach((item, ind) => {
    item.addEventListener('click', () => {
        hidden();
        show(ind);
        heder.textContent = item.innerText;
        addId();
        for (const key in sabHeaders) {
            if (item.getAttribute('data-id') === key) {
                cubHeder.textContent = sabHeaders[key];
            }
        }
    });
});

function show(ind = 0) {
    tabs[ind].classList.add('selected');
}

function hidden() {
    tabs.forEach(item => {
        item.classList.remove('selected');
    });
}

function addId() {
    tabs.forEach(item => {
        if (item.classList.contains('selected')) {
            payment.id = item.getAttribute('data-id');
        }
    });
    metersAdd();
}

function metersAdd() {
    let result ='';
    const ary = [];
    meters.forEach(elem => {
        if (meters.value === elem.value && meters.value !== null) {
            result = elem.value;
        }
    });
    if (payments.length > 0) {
        payments.forEach(item => {
            if (item.id === payment.id) {
                ary.push(item);
            }
        });
        if (ary.length > 0) {
            ary.forEach(elem => {
                if (elem.meterId === result) {
                    payment.meterId = null;
                }else {
                    payment.meterId = result;
                }
            });
        } else {
            payment.meterId = result;
        }
    }
    if (payments.length === 0) {
        payment.meterId = result;
    }
}



meters.addEventListener('input', () => {
    metersAdd();
});

previous.addEventListener('input', () => {
    if (+previous.value > 0 && typeof +previous.value === 'number') {
        payment.previous = +previous.value;
    }
});

current.addEventListener('input', () => {
    if (+current.value > previous.value && typeof +current.value === 'number') {
        payment.current = +current.value;
    } else {
        payment.current = null;
        payment.amount = null;
        return;
    }
    let out;
    for (const key in tariffs) {
        if (key === payment.id) {
            out = (payment.current - payment.previous) * tariffs[key];
            payment.amount = +out.toFixed(2);
        }
    }
});


function domList () {
    let out = '';
    let sum = 0;
    payments.forEach(element => {
        out += `
            <li class="list__item">
                <p><span class="list__item-label">${element.meterId}</span>
                    <span class="price">$ <b>${element.amount}</b></span>
                </p>
            </li>
        `;

    });
    wrapList.innerHTML = out;
    for (let i = 0; i < payments.length; i++) {
        sum += payments[i].amount;
    }
    wrapList.insertAdjacentHTML('beforeend', `
    <li class="list__item list__total">
        <p><span class="list__item-label">Всего</span>
        <span class="price">$ <b>${sum.toFixed(2)}</b></span>
        </p>
    </li>
    `);
};




function addChecked() {
    let out = '';
    let count = 0;
    payments.forEach((item, i )=> {
        if (i === 0 || item.id !== payments[count].id) {
            out += `
                <p class="right__payments-field">
                    <label>
                        <input value="${item.id}" type="checkbox" checked/>
                        <span>${item.heder}</span>
                    </label>
                </p>
            `; 
            count = i;
        }
    });
    saveId.innerHTML = out;
};

btnPay.addEventListener('click', (e) => {
    e.preventDefault();
    let checks = document.querySelectorAll('[type="checkbox"]');
    let out = '';
    let text = '';
    checks.forEach(item => {
        text = item.parentNode.childNodes[3].innerText;
        for (let i = 0; i < payments.length; i++) {
            if (item.value === payments[i].id && item.checked === true) {
                balance -=  payments[i].amount;

            }
        }            
        if (balance >= 0) {
            payments.splice(0, payments.length);
            out += `
            <li class="list__item">${text}: успешно оплачено</li>
            `;
        }
        if(balance < 0){
            out += `
            <li class="list__item list__item-error">${text}: ошибка транзакции</li>
            `;
        }
    });
    transactions.innerHTML = out;
    console.log(balance);
    localStorage.setItem('payments', JSON.stringify(payments));
    domList ();
    addChecked();
});


btnSave.addEventListener('click', (e) => {
    e.preventDefault();
    metersAdd();
    let save;
    for (const key in payment) {
        if (payment[key] !== null) {
            save = true;
        } else {
            return save = false;
        }
    }
    if (save === true) {
        payment.heder = heder.textContent;
        payments.push( Object.assign({},payment));
        domList ();
        addChecked();
        payment.amount = null;
        payment.current = null;
        current.value = '';
        previous.value = '';
        localStorage.setItem('payments', JSON.stringify(payments));
    }
});

btnReset.addEventListener('click', ()=> {
    payments.splice(0, payments.length);
    domList();
    addChecked();
    localStorage.setItem('payments', JSON.stringify(payments));
});
