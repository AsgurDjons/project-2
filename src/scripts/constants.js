export const tariffs = Object.freeze({
    taxes: 0.2,
    water: 0.3,
    internet: 0.4,
    security: 0.5,
    exchange: 0.6,
});

export const sabHeaders = Object.freeze({
    taxes: 'Налогов',
    water: 'Холодного водоснабжения',
    internet: 'Интернета',
    security: 'Охраны',
    exchange: 'Обмена валют',
});

